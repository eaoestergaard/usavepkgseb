#' Returns up- and down´savingsplan
#'
#' @param changeOfConsumption Annual change of consumption
#' @param economicNetWorth last year in calculation
#' @param valuationRate Valuation rate
#' @param currentAge Currrent age in years
#' @param expectedAgeOfDeath Expected dead age
#' @export
#' @examples
#' model1OptimalConsumption(changeOfConsumption = 0.01,economicNetWorth=1300000,valuationRate=0.03,currentAge=25,expectedAgeOfDeath=90)
model1OptimalConsumption <- function(changeOfConsumption = 0,
                                     economicNetWorth = 0,
                                     valuationRate = 0,
                                     currentAge = 0,
                                     expectedAgeOfDeath = 0){
  optimalConsumption = economicNetWorth/((1-((1+changeOfConsumption)^(expectedAgeOfDeath-currentAge)*(1+valuationRate)^(currentAge-expectedAgeOfDeath)))/(valuationRate-changeOfConsumption))
  return(optimalConsumption)
}
