#' Returns up- and down´savingsplan
#'
#' @param minAge first year in calculation
#' @param maxAge last year in calculation
#' @param currentAge Currrent age in years
#' @param retirementAge Retirement age in years. Must be larger than currentAge
#' @param expectedAgeOfDeath Expected dead age
#' @param grossHumanCapital Gross human Capital
#' @param financialCapital Financial Capital
#' @param economicNetWorth Economic NET worth
#' @param salary Current after tax salary
#' @param growthRateOfSalary Annual growth rate of salary
#' @param consumption Annual consumption
#' @param changeOfConsumption Annual change of consumption
#' @param valuationRate Valuation rate
#' @import stats
#' @import utils
#' @export
#' @examples
#' consumptionSmoothing(minAge = 25,maxAge=90,currentAge=25,retirementAge=65,expectedAgeOfDeath = 90,grossHumanCapital=0,financialCapital=3000,economicNetWorth=1372536,salary=50500,growthRateOfSalary=0.01,consumption = 38185.8168481200,changeOfConsumption = 0.01,valuationRate = 0.03)
consumptionSmoothing<- function(minAge = 25,
                                maxAge=90,
                                currentAge=25,
                                retirementAge=65,
                                expectedAgeOfDeath = 90,
                                grossHumanCapital=0,
                                financialCapital=3000,
                                economicNetWorth=1372536,
                                salary=50500,
                                growthRateOfSalary=0.01,
                                consumption = 38185.8168481200,
                                changeOfConsumption = 0.01,
                                valuationRate = 0.03){
  if (expectedAgeOfDeath < retirementAge) {
    return(NULL)
  }

  ages = currentAge:expectedAgeOfDeath
  agesToPension = currentAge:(retirementAge-1)
  agesAtPension = retirementAge:expectedAgeOfDeath

  range=minAge:maxAge

  salaryCalc = rep(0,maxAge)
  consumptionCalc = rep(0,maxAge)
  savingsCalc = rep(NA,maxAge)
  financialCapitalCalcP = rep(NA,maxAge)
  grossHumanCapitalCalcP = rep(NA,maxAge)
  financialCapitalCalcU = rep(NA,maxAge)
  economicNetWorthU = rep(NA,maxAge)
  economicNetWorthP = rep(NA,maxAge)


  salaryCalc[agesToPension] = salary*cumprod(c(1,rep((1+growthRateOfSalary),length(agesToPension)-1)))
  consumptionCalc[ages] = consumption*c(1,cumprod(rep((1+changeOfConsumption),length(ages)-1)))

  savingsCalc = salaryCalc - consumptionCalc
  salaryCalc[salaryCalc == 0] <- NA
  consumptionCalc[consumptionCalc == 0] <- NA
  savingsCalc[savingsCalc == 0] <- NA

  if (length(ages)>1) {
  for (t in ages){
    if (t == ages[1] ) {
      #Initial values
      financialCapitalCalcP[t] = financialCapital
    }else {
      financialCapitalCalcP[t] = financialCapitalCalcU[t-1]
    }
    grossHumanCapitalCalcP[t] = calculateHumanCapital(annualAfterTaxSalary=salaryCalc[t],
                                                   growthRateOfSalary=growthRateOfSalary,
                                                   valuationRate=valuationRate,
                                                   currentAge=t,
                                                   retirementAge=retirementAge)
    #Ultimo values
    financialCapitalCalcU[t] = financialCapitalCalcP[t] * (1+valuationRate) + savingsCalc[t]
    economicNetWorthU[t] = consumptionCalc[t]*((1-(1+valuationRate)^(t+1-expectedAgeOfDeath))/valuationRate)
    if (t == tail(ages,1)) {
      financialCapitalCalcU[t] = NA
      economicNetWorthU[t] = NA
    }
  }}

  grossHumanCapitalCalcU = c(grossHumanCapitalCalcP[-1],0)
  economicNetWorthP[ages] = c(economicNetWorth,head(economicNetWorthU[ages],-1))
  consumptionSmoothingCalc <- new.env()
  consumptionSmoothingCalc$ages = range
  consumptionSmoothingCalc$grossHumanCapitalCalcP = grossHumanCapitalCalcP[range]
  consumptionSmoothingCalc$financialCapitalCalcP = financialCapitalCalcP[range]
  consumptionSmoothingCalc$economicNetWorthP = economicNetWorthP[range]
  consumptionSmoothingCalc$salaryCalc = salaryCalc[range]
  consumptionSmoothingCalc$consumptionCalc = consumptionCalc[range]
  consumptionSmoothingCalc$savingsCalc = savingsCalc[range]
  consumptionSmoothingCalc$grossHumanCapitalCalcU = grossHumanCapitalCalcU[range]
  consumptionSmoothingCalc$financialCapitalCalcU = financialCapitalCalcU[range]
  consumptionSmoothingCalc$economicNetWorthU = economicNetWorthU[range]
  return(consumptionSmoothingCalc)
}



